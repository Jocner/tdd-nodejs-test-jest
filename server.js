const express = require('express');
const axios = require('axios');
const app = express();
const port = 4000;
const { users } = require('./endpoints');

app.use(express.json({ extended: true }));

const usersHandlers = users({ axios });

app.get('/', usersHandlers.get);

app.post('/', usersHandlers.post);

app.put('/:id', usersHandlers.put);

app.delete('/:id', usersHandlers.delete);



app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});